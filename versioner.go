package NewVer

import (
	"errors"
	"fmt"
)


func Greet(name,surname,role,respect string ) (string,error) {
		switch respect {
	case "1":
		return fmt.Sprintf("ah its you, %s %s,you are pathetic %s i known", name,surname,role), nil
	case "2":
		return fmt.Sprintf("Hi, %s %s,i heard you are %s ", name,surname,role), nil
	case "3":
		return fmt.Sprintf("Hello, %s %s,you are %s isnt it?", name,surname,role), nil
	case "4":
		return fmt.Sprintf("Hi mister, %s %s,you are greatest %s i known", name,surname,role), nil
	default:
		return "", errors.New("unknown respect gradation, write number from 1 to 4")
	}
}

